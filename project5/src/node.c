#include "node.h"

int DEBUG = 0, DYNAMIC = 0, LSP_SEQ = 1, totalNodes = 0, knownNodes = 0, numNeighbors = 0, receivedLSPCount = 0;
char *nodeLabel;

Neighbor *neighbors;
Node *networkGraph;

int main(int argc, char *argv[]) {
	char *discoverFile, buf[BUFFER_SIZE] = {0};
	int sock, portNum, i; // Socket FD
	
	if (argc < 4) {
		fprintf(stderr, "usage: %s nodeLabel portNum totalNumNodes discoverFile [-dynamic | -debug]\n", argv[0]);
		exit(1);
	}

	// Open a new socket on our machine
	sock = openSocket();

	// Initialize some things
	nodeLabel = argv[1];
	portNum = atoi(argv[2]);
	totalNodes = atoi(argv[3]);
	discoverFile = argv[4];
	srand(time(NULL));

	if ((argc > 4 && (strcmp(argv[4], "-debug") == 0)) ||
			(argc > 5 && (strcmp(argv[5], "-debug") == 0)))
		DEBUG = 1;
	
	if ((argc > 4 && (strcmp(argv[4], "-dynamic") == 0)) ||
	    (argc > 5 && (strcmp(argv[5], "-dynamic") == 0)))
		DYNAMIC = 1;

	if (DEBUG) {
		int countdown = 30;
		fprintf(stderr, "Killing after %is\n", countdown);
		createThread(suicide, (void *) &countdown);
	}
	
	// Create network graph
	networkGraph = malloc(sizeof(struct node) * totalNodes);

	// Get neighbor information
	neighbors = getNeighbors(discoverFile);
	
	// Add ourselves to the graph
	networkGraph[knownNodes] = newNode(nodeLabel);
	networkGraph[knownNodes++]->edges = parseEdges(networkGraph[0], buildCostData(buf, neighbors, numNeighbors));
	
	// Add our neighbors to the graph
	for (i = 1; i < numNeighbors + 1; i++) {
		networkGraph[i] = newNode(neighbors[i - 1]->label);
		knownNodes++;
	}
	printGraph(networkGraph);

	// Create address to listen on
	struct sockaddr_in *addr = listenAddr(portNum);

	// Bind our socket to the address we are listening on
	if (bind(sock, (const struct sockaddr *) addr, sizeof(*addr)) < 0)
		fatalError("Error binding to address");
	fprintf(stderr, "Listening on port %i\n", portNum);
	
	
	// Send LSPackets in the background
	createThread(flood, (void *) &sock);
	
	if (DYNAMIC) 
		createThread(updateCost, NULL);
	
	listenForPackets(sock);

	// Gracefully exit
	printf("Goodbye!\n");
	return 0;
}

/**
* Opens a UDP socket
*/
int openSocket() {
	int sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0)
		fatalError("Error opening socket");
	return sock;
}

void *flood(void *data) {
	int i, sock = *(int *) data; 
	char packetData[BUFFER_SIZE], costData[BUFFER_SIZE];

	while (1) {
		memset(costData, 0, BUFFER_SIZE);
		memset(packetData, 0, BUFFER_SIZE);
		fprintf(stderr, "---> Flooding...\n");
		
		// Build packet
		buildCostData(costData, neighbors, numNeighbors);
		buildLSP(packetData, nodeLabel, totalNodes, LSP_SEQ++, costData);

		for (i = 0; i < numNeighbors; i++) {
			fprintf(stderr, "Sending LSP to %s\n", neighbors[i]->label);

			if (sendLSPacket(sock, &(neighbors[i]->addr), packetData) < 0)
				fatalError("flood");
		}
		
		throttle(THROTTLE_FLOODING);
	}
	
	return NULL;
}

void *suicide(void *data) {
	int countdown = *(int *) data;
	
	// Wait for 'countdown' seconds
	throttle(countdown * 1000000);
	
	// Then commit the deed
	exit(0);
	
	return NULL;
}

void *updateCost(void *data) {
	int random;
	
	while (1) {
		// Wait for 1s
		throttle(THROTTLE_UPDATE_COST);
		
		// 25% chance to change cost
		if ((random = randomInt()) % 4 == 0) {
			// Choose one neighbor to randomly change cost
			neighbors[random % numNeighbors]->cost = abs(randomInt() % NODE_COST_LIMIT) + 1;
		}
	}
}

/**
* Listen for incoming packets
*/
void listenForPackets(int sock) {
	char buf[BUFFER_SIZE];
	struct sockaddr_in peerAddr;
	socklen_t size = sizeof(peerAddr);
	
	LSPacket packet = malloc(sizeof(struct lspacket));

	// Keep listening until we have left the BB Network
	while (1) {
		memset(buf, 0, BUFFER_SIZE);
		if (recvfrom((int) sock, buf, BUFFER_SIZE, 0, (struct sockaddr *) &peerAddr, &size) < 0)
			fatalError("Error while receiving data");

		// Got a UDP packet
		processPacket(parseLSPacket(packet, buf, peerAddr), sock);
	}
}

/**
* We have received a packet and need to perform an action
*/
void processPacket(LSPacket packet, int sock) {
	
	Node node;
	Neighbor neighbor;
	int i, j, nodesWithEdges = 0;
	
	fprintf(stderr, "Got LSP from %s\tTTL: %i\tSeq: %i\n%s\n", packet->label, packet->ttl, packet->seqNum, packet->costData);
	
	if (strcmp(packet->label, nodeLabel) == 0) {
		// Ignore the packet, it's from ourselves
		fprintf(stderr, "Dropping packet: packet is ours\n");
		return;
	}
	// Test and decrement the TTL
	if (packet->ttl < 1) {
		fprintf(stderr, "Dropping packet: TTL expired\n");
		// Drop the packet, the ttl is 0
		return;
	}
	
	// Is this a new node?
	if ((node = getNode(packet->label, networkGraph)) == NULL) {
		fprintf(stderr, "Found new node, adding to the network: %s\n", packet->label);
		
		// Create a new node
		node = newNode(packet->label);
		node->seqNum = packet->seqNum;

		// Attach to network graph
		networkGraph[knownNodes++] = node;
	}
	else if (packet->seqNum > node->seqNum) {
		// Update sequence number
		node->seqNum = packet->seqNum;

		// Remove old edges
		for (i = 0; i < node->edgeCount; i++) {
			freeEdge(node->edges[i]);
		}
		free(node->edges);
		
		fprintf(stderr, "Updating costs for Node %s\n", packet->label);
	}

	// Attach new edges
	node->edges = parseEdges(node, packet->costData);

	// If this node is our neighbor, we need to update our cost information to it
	if ((neighbor = getNeighbor(node->label)) != NULL) {
		for (j = 0; j < node->edgeCount; j++) {

			// Does this node edge connect us to our neighbor?
			if (strcmp(node->edges[j]->label, nodeLabel) == 0) {
				// Update the cost
				neighbor->cost = node->edges[j]->cost;
				fprintf(stderr, "Updated cost to neighbor: %s -> %i\n", neighbor->label, neighbor->cost);
			}
		}
	}

	// Forward the LSPacket
	forwardLSPacket(sock, neighbors, numNeighbors, packet);

	// Have we found all nodes?
	for (i = 0; i < knownNodes; i++) {
		if (networkGraph[i]->edgeCount > 0)
			nodesWithEdges++;
	}

	if (nodesWithEdges == totalNodes) {
		// Run Dijkstra's algorithm
		dijkstra(networkGraph, getNode(nodeLabel, networkGraph));
		
		printGraph(networkGraph);
	}
}

void dijkstra(Node *graph, Node src) {
	int i, j;
	Node node, updateNode;
	
	for (i = 0; i < totalNodes; i++) {
		node = graph[i];
		// Initialize all nodes to unknown with infinite cost
		node->distance = INT32_MAX;
		node->isKnown = 0;

		// Initialize source node
		if (isSameNode(node, src)) {
			src->distance = 0;
		}
	}

	
	for (i = 0; i < totalNodes; i++) {
		// Get the next closest node
		node = getClosestNode(graph);
		
		// If there are no more unknown nodes, we are done
		if (node == NULL)
			return;

		// Mark the node as processed
		node->isKnown = 1;
		
		for (j = 0; j < totalNodes; j++) {
			updateNode = graph[j];
			
			if (!updateNode->isKnown && isNeighbor(node, updateNode) &&
					node->distance + findCost(node, updateNode) < updateNode->distance) {
				updateNode->distance = node->distance + findCost(node, updateNode);
			}
		}
	}
}

Node getClosestNode(Node *graph) {
	int i, min = INT32_MAX;
	Node node = NULL;
	
	for (i = 0; i < totalNodes; i++) {
		if (!graph[i]->isKnown && graph[i]->distance < min) {
			min = graph[i]->distance;
			node = graph[i];
		}
	}
	
	return node;
}

Edge getEdge(Node node, char *label) {
	int i;
	for (i = 0; i < node->edgeCount; i++) {
		if (strcmp(label, node->edges[i]->label))
			return node->edges[i];
	}
	return NULL;
}

int isSameNode(Node node1, Node node2) {
	return strcmp(node1->label, node2->label) == 0;
}

int isNeighbor(Node node1, Node node2) {
	int i;
	
	for (i = 0; i < node1->edgeCount; i++) 
		if (strcmp(node1->edges[i]->label, node2->label) == 0)
			return 1;
	
	return 0;
}

int findCost(Node node1, Node node2) {
	int i;
	
	if (isSameNode(node1, node2))
		return 0;

	for (i = 0; i < node1->edgeCount; i++)
		if (strcmp(node1->edges[i]->label, node2->label) == 0)
			return node1->edges[i]->cost;

	return INT32_MAX;
}


void printGraph(Node *graph) {
	int i = 0, j;
	Node node;
	Edge edge;

	fprintf(stderr, "------------\n");
	
	for (i = 0; i < knownNodes; i++) {
		node = graph[i];
		fprintf(stderr, "%s: Distance = %i\n", node->label, node->distance);

		if (node->edges != NULL) {
			
			for (j = 0; j < node->edgeCount; j++) {
				edge = node->edges[j];
				fprintf(stderr, "  %s -> %i\n", edge->label, edge->cost);
			}
		}
	}
	
	fprintf(stderr, "------------\n");
}

Edge *parseEdges(Node node, char *edgeData) {
	Edge edgeContainer[NODE_EDGE_LIMIT]; 
	char *token, *label, *cost;
	node->edgeCount = 0;
	
	token = strtok(strdup(edgeData), ":");
	while (token != NULL) {
		// Get the label and cost
		label = strtok(NULL, ",");
		cost = strtok(NULL, "\n");

		Edge edge = newEdge(label, atoi(cost));
		edgeContainer[node->edgeCount++] = edge;
		token = strtok(NULL, ":");
	}
	
	Edge *edges = malloc(sizeof(struct edge*) * node->edgeCount);
	int i;
	for (i = 0; i < node->edgeCount; i++) {
		edges[i] = edgeContainer[i];
	}
	
	return edges;
}

Node getNode(char *label, Node *graph) {
	int i;
	for (i = 0; i < knownNodes; i++) {
		if (strcmp(label, graph[i]->label) == 0)
			return graph[i];
	}
	return NULL;
}

Neighbor getNeighbor(char *label) {
	int i;
	for (i = 0; i < numNeighbors; i++) {
		if (strcmp(label, neighbors[i]->label) == 0)
			return neighbors[i];
	}
	return NULL;
}

Neighbor *getNeighbors(char *filename) {
	long fileLen;
	char *fileStr;
	FILE *fileP;

	// Open the file in read+write mode, and create the file if it does not exist
	if ((fileP = fopen(filename, "r")) == NULL)
		fprintf(stderr, "Error reading shared file");

	fseek(fileP, 0, SEEK_END); // Move pointer to end of file
	fileLen = ftell(fileP); // How many long is the file?
	fseek(fileP, 0, SEEK_SET); // Move pointer to beginning of file

	// Allocate enough memory for the file contents
	fileStr = malloc(sizeof(char) * (fileLen + 1));
	// Clear the memory
	memset(fileStr, 0, fileLen + 1);

	// Read file and close it
	fread(fileStr, 1, fileLen, fileP);
	fclose(fileP);

	// Parse string into messages
	Neighbor *neighbors = parseNeighbors(fileStr);
	free(fileStr);

	// We have read the file and no longer need the token
	return neighbors;
}

Neighbor *parseNeighbors(char *fileStr) {
	char *buf, *nodeLabel, *hostname, *portNum, *cost;
	int memLen = (sizeof(struct neighbor) * totalNodes);

	// Allocate memory for message array
	Neighbor *neighbors = malloc(memLen);
	memset(neighbors, 0, memLen);

	// Tokenize string
	buf = strtok(fileStr, ",\n");
	while (buf != NULL) {
		nodeLabel = buf;
		hostname = strtok(NULL, ",\n");
		portNum = strtok(NULL, ",\n");
		cost = strtok(NULL, ",\n");

		neighbors[numNeighbors++] = newNeighbor(nodeLabel, hostname, atoi(portNum), atoi(cost));
		buf = strtok(NULL, ",\n");
	}

	return neighbors;
}

Neighbor newNeighbor(char *label, char *hostname, int port, int cost) {
	Neighbor neighbor = malloc(sizeof(struct neighbor));
	int labelLen = strlen(label) + 1;
	int hostLen = strlen(hostname) + 1;

	// Copy data to message
	neighbor->label = malloc(sizeof(char) * labelLen);
	strncpy(neighbor->label, label, labelLen + 1);

	neighbor->hostname = malloc(sizeof(char) * hostLen);
	strncpy(neighbor->hostname, hostname, hostLen + 1);

	neighbor->port = port;
	neighbor->cost = cost;
	
	struct sockaddr_in *addr = connectToAddr(neighbor->hostname, neighbor->port);
	neighbor->addr = *addr;

	return neighbor;
}

Node newNode(char *label) {
	Node node = malloc(sizeof(struct node));
	int labelLen = strlen(label) + 1;
	
	// Initialize sequence number
	node->seqNum = 0;

	// Copy data to message
	node->label = malloc(sizeof(char) * labelLen);
	strncpy(node->label, label, labelLen);
	
	node->distance = 0;
	node->isKnown = 0;
	
	node->edges = NULL;
	node->edgeCount = 0;

	return node;
}

Edge newEdge(char *label, int cost) {
	Edge edge = malloc(sizeof(struct edge));
	int labelLen = strlen(label) + 1;

	// Copy data to message
	edge->label = malloc(sizeof(char) * labelLen);
	strncpy(edge->label, label, labelLen);
	edge->cost = cost;

	return edge;
}

void freeEdge(Edge edge) {
	free(edge->label);
	free(edge);
}

/**
* Pulls entropy from /dev/urandom
* Generates a 32-bit random int
* Comparable to the IPv4 address space
*/
int randomInt() {
	FILE *prng;
	int random;

	if ((prng = fopen("/dev/urandom", "r")) == NULL)
		fatalError("Error accessing PRNG");

	fread(&random, 1, sizeof(int), prng); // Grab some random data
	fclose(prng);

	return random;
}

// Sleep for 250ms to throttle network usage to a reasonable speed
// and prevent kernel network buffer overflows 
void throttle(int micros) {
	usleep(micros);
}