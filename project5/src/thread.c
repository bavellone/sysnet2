/**
* @file thread.c
* @brief Wrapper for thread creation
*
* @author Ben Avellone
* @author Hunter Hardy
*
*/

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include "thread.h"

/**
* @brief Creates the threads managed by the ThreadPool tm with the given
* entry-point function and data.
*/
pthread_t createThread(void *function, void *data) {
	int err;
	pthread_t thread;
	pthread_attr_t attr;

	// Set default attributes
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	
	// Create thread
	err = pthread_create(&thread, &attr, function, data);

	if (err) {
		fprintf(stderr, "Failed to create new thread!");
	}

	pthread_attr_destroy(&attr);

	return thread;
}

/**
* @brief Joins on all threads managed by the given ThreadPool
*/
void joinThread(pthread_t thread) {
	pthread_join(thread, NULL);
}
