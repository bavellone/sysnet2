/*
	A set of common headers and functions to be shared between the server and peers
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>

#include "thread.h"

#define BUFFER_SIZE 1024
#define THROTTLE_FLOODING 5000000
#define THROTTLE_UPDATE_COST 10000000
#define NODE_EDGE_LIMIT 16
#define NODE_COST_LIMIT 20

typedef struct edge {
	int cost;
	char *label;
} *Edge;

typedef struct node {
	int seqNum;
	int edgeCount;
	char *label;
	Edge *edges;
	
	// For Dijkstra's
	int isKnown;
	int distance;
} *Node;

typedef struct neighbor {
	char *label;
	int cost;
	
	char *hostname;
	int port;
	struct sockaddr_in addr;
} *Neighbor;

typedef struct lspacket {
	int ttl;
	int seqNum;
	char *label;

	char *costData;

	int port;
	char IPaddr[BUFFER_SIZE];
	char *rawData;
	struct sockaddr_in from;
} *LSPacket;

LSPacket parseLSPacket(LSPacket packet, char *, struct sockaddr_in);
int sendLSPacket(int, struct sockaddr_in *, char *);
char *buildLSP(char *, char *, int, int, char *);
char *buildCostData(char *, Neighbor *, int);
struct sockaddr_in *connectToAddr(char *, int);
struct sockaddr_in *listenAddr();
Neighbor *parseNeighbors(char *);
Neighbor newNeighbor(char *, char *, int, int);
Neighbor *getNeighbors(char *);
Neighbor getNeighbor(char *);
Edge *parseEdges(Node, char *);
void forwardLSPacket(int, Neighbor *, int, LSPacket);
Node newNode(char *);
Node getNode(char *, Node *);
Edge newEdge(char *, int);
int isSameAddr(struct sockaddr_in *, struct sockaddr_in *);
void fatalError(char *);
int randomInt();
void freeEdge(Edge);
int bindSocket(struct sockaddr_in *);
int getPort(int, struct sockaddr_in *);
int openSocket();
void runTests();
void listenForPackets(int);
void processPacket(LSPacket, int);
void *flood(void *);
void *suicide(void *);
void *updateCost(void *);
int randomInt();
void clearBuffer();
void throttle();
void printGraph(Node *);
Node getClosestNode(Node *);
void dijkstra(Node *, Node);
int isSameNode(Node, Node);
int isNeighbor(Node, Node);
int findCost(Node, Node);
Edge getEdge(Node, char *);