#include "node.h"

/**
* Parses the given buffer and assigns the packet type and data to the packet struct
*/
LSPacket parseLSPacket(LSPacket packet, char *buffer, struct sockaddr_in peerAddr) {
	// LSPacket format
	// ttl
	// seqNum
	// label
	// Cost Data

	// Grab packet data
	packet->rawData = strdup(buffer);
	packet->label = strdup(strtok(buffer, ",")); // The first line is the label
	packet->ttl = atoi(strtok(NULL, ",")); // The type is the first character
	packet->seqNum = atoi(strtok(NULL, "\n")); // The data starts at the third character, after the colon

	packet->costData = strdup(strtok(NULL, ""));
	
	// Save packet connection information
	inet_ntop(AF_INET, (void *) &(peerAddr.sin_addr), packet->IPaddr, (socklen_t) BUFFER_SIZE);
	packet->port = ntohs(peerAddr.sin_port);
	packet->from = peerAddr;

	return packet;
}

/**
* Creates a buffer for the given packet type and data
*/
char *buildLSP(char *buf, char *label, int ttl, int seqNum, char *costData) {
	// Write packet data to buffer
	snprintf(buf, BUFFER_SIZE, "%s,%i,%i\n%s", label, ttl, seqNum, costData);
	return buf;
}

char *buildCostData(char *buf, Neighbor *neighbors, int neighborCount) {
	char costStr[12] = {0};
	int i;

	// Get neighbor cost information
	for (i = 0; i < neighborCount; i++) {
		memset(costStr, 0, 12);
		snprintf(costStr, 12, "cost:%s,%i\n", neighbors[i]->label, neighbors[i]->cost);
		strcat(buf, costStr);
	}

	fprintf(stderr, "Built cost data: %s\n", buf);
	
	return buf;
}

/**
* Low-level function to send packet to an address
*/
int sendLSPacket(int sockFD, struct sockaddr_in *addr, char *data) {
	socklen_t addrLen = sizeof(*addr);
	return sendto(sockFD, data, strlen(data) + 1, 0, (struct sockaddr *) addr, addrLen);
};

void forwardLSPacket(int sock, Neighbor *neighbors, int neighborCount, LSPacket packet) {
	char packetData[BUFFER_SIZE];
	int i;

	buildLSP(packetData, packet->label, packet->ttl - 1, packet->seqNum, packet->costData);
	
	for (i = 0; i < neighborCount; i++) {
		
		// Don't send back to the node we received this packet from
		if (!(isSameAddr(&(neighbors[i]->addr), &(packet->from)))) {
			
			if (sendLSPacket(sock, &(neighbors[i]->addr), packetData) < 0)
				fatalError("forwardLSPacket");
			
			fprintf(stderr, "Forwarded LSP to %s\n", neighbors[i]->label);
		}
	}
}

/**
* Used to create an address struct from an IP address and port
*/
struct sockaddr_in *connectToAddr(char *hostname, int port) {
	struct sockaddr_in *addr = malloc(sizeof(struct sockaddr_in));
	struct hostent *host;

	if ((host = gethostbyname(hostname)) == NULL)
		fatalError("gethostbyname");

	// Set up server address
	memset(addr, 0, sizeof(*addr));
	addr->sin_family = AF_INET;
	addr->sin_port = htons(port);
	addr->sin_addr = *(struct in_addr *) host->h_addr;

	return addr;
}

/**
* Used to create an address struct meant for listening
*/
struct sockaddr_in *listenAddr(int port) {
	struct sockaddr_in *addr = malloc(sizeof(struct sockaddr_in));

	// Set up server address
	memset(addr, 0, sizeof(*addr));
	addr->sin_family = AF_INET;
	addr->sin_addr.s_addr = htonl(INADDR_ANY);
	addr->sin_port = htons(port);
	
	return addr;
}

int isSameAddr(struct sockaddr_in *addr1, struct sockaddr_in *addr2) {
	char IPaddr1[BUFFER_SIZE], IPaddr2[BUFFER_SIZE];

	inet_ntop(AF_INET, (void *) &(addr1->sin_addr), IPaddr1, (socklen_t) BUFFER_SIZE);
	inet_ntop(AF_INET, (void *) &(addr2->sin_addr), IPaddr2, (socklen_t) BUFFER_SIZE);

	// Returns true if both IP addresses and ports match
	return ((strcmp(IPaddr1, IPaddr2) == 0) && (ntohs(addr1->sin_port) == ntohs(addr2->sin_port)));
}

/**
* Helper function to print error messages
*/
void fatalError(char *err) {
	perror(err);
	exit(1);
}

/**
* Clears the buffer of unwanted newline chars
*/
void clearBuffer() {
	while (getchar() != '\n');
}

int bindSocket(struct sockaddr_in *sendAddr) {
	// Open a new socket
	socklen_t sendLen = sizeof(*sendAddr);

	int sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0)
		fatalError("Error opening socket");

	// Bind to address
	if (bind(sock, (struct sockaddr *) sendAddr, sendLen) < 0)
		fatalError("Error binding to address");

	return sock;
}

int getPort(int sock, struct sockaddr_in *sendAddr) {
	socklen_t size = sizeof(struct sockaddr);
	getsockname(sock, (struct sockaddr *) sendAddr, &size);
	return ntohs(sendAddr->sin_port);
}