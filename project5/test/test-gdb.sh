#!/bin/sh

set -e

echo "Re-compiling..."
make -C ../src clean
make -C ../src all

echo "Running tests..."
../src/node A 8080 3 A.net -dynamic -debug &> A.log & 
echo $!
../src/node B 8081 3 B.net -dynamic -debug &> B.log &
echo $!