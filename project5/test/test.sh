#!/bin/sh

set -e

echo "Re-compiling..."
make -C ../src clean
make -C ../src all

echo "Running tests..."
../src/node A 8080 3 A.net -dynamic &> A.log & 
echo $!
../src/node B 8081 3 B.net -dynamic &> B.log &
echo $!
../src/node C 8082 3 C.net -dynamic &> C.log