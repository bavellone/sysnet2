/*
	A set of common headers and functions to be shared between the server and peers
 */

#ifndef COMMON

#define COMMON 1

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

#define BUFFER_SIZE 1024
#define HEADER_LENGTH 3
#define DATA_LENGTH 7

// Used to determine packet type
#define PACKET_TYPE_ANNOUNCE 0
#define PACKET_TYPE_WAITING_PEERS 1
#define PACKET_TYPE_NEIGHBOR_ADDR 2
#define PACKET_TYPE_LEADER_ELECTION 3
#define PACKET_TYPE_TOKEN 4

typedef struct comm_packet {
	int type;
	char *data;
	
	int port;
	char IPaddr[BUFFER_SIZE];

	struct sockaddr_in from;
} *Comm_Packet;

typedef struct peer {
	char *IPaddr;
	int port;
	struct sockaddr_in sockAddr;
} *Peer;

Comm_Packet parseCommPacket(Comm_Packet , char *, struct sockaddr_in);
int sendToPeer(Peer, int, int, char *);
int sendCommPacket(int, int, struct sockaddr_in, char *);
char *buildPacketData(int, char *);
Peer newPeerFromPacket(Comm_Packet);
Peer newPeer(char *, int, struct sockaddr_in);
void freePeer(Peer);
struct sockaddr_in *connectToAddr(char *, int);
struct sockaddr_in *listenAddr();
void fatalError(char *);
void clearBuffer();
int bindSocket(struct sockaddr_in *);
int getPort(int, struct sockaddr_in *);

#endif