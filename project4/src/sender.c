#include "common.h"
#include "rdtSender.h"


int main(int argc, char *argv[]) {
	if (argc < 3) {
		fprintf(stderr, "Usage: %s proxyHostName proxyPort\n", argv[0]);
		exit(1);
	}

	char msg[BUFFER_SIZE];

	// Loop forever
	while (1) {
		puts("Enter a message:");
		// Read the users message
		fgets(msg, BUFFER_SIZE, stdin);
		if (!sendMessage(argv[1], atoi(argv[2]), (char *)&msg))
			fatalError("Couldn't send message to receiver!");
		else
			printf("Message sent!\n");
	}

	return 0;
}


/*
 * Sends a message to the destination IP address and port
 */
int sendMessage(char *desthost, int destPort, char *message) {
	struct sockaddr_in *destAddr = connectToAddr(desthost, destPort);
	struct sockaddr_in *sendAddr = listenAddr();
	int sock = bindSocket(sendAddr);

	return sendPackets(message, sock, sendAddr, destAddr);
}


/*
 * Sends the given message in one or more packets to the receiver/proxy
 */
int sendPackets(char *message, int sock, struct sockaddr_in *sendAddr, struct sockaddr_in *destAddr) {
	int seqNum = 0, offset = 0;
	char sendBuf[BUFFER_SIZE];
	
	while (offset < strlen(message)) {
		memset(sendBuf, 0, BUFFER_SIZE);
		snprintf(sendBuf, BUFFER_SIZE, "0%i%i", seqNum, (offset + DATA_LENGTH >= strlen(message)));
		strncpy(&sendBuf[3], &(message[offset]), DATA_LENGTH);
		
		sendFragment(seqNum, sock, sendBuf, destAddr);
		
		seqNum = (seqNum + 1) % 2;
		
		offset += DATA_LENGTH;
		if (offset > strlen(message))
			offset = strlen(message);
	}
	
	return 1;
}

/*
 * Sends a single fragment of data at a time. Will retransmit fragment as necessary.
 * Blocks until fragment is sent.
 */
void sendFragment(int seqNum, int sock, char *data, struct sockaddr_in *destAddr) {
	int sentAck = -1, rv = 0;
	socklen_t destLen = sizeof(*destAddr);
	fd_set readfds;
	struct timeval tv;
	

	// Keep transmitting until we have received a valid ACK from the receiver
	while (sentAck != seqNum) {
		// Only send if we haven't sent this packet yet or if the timeout elapsed
		if (!rv && sendto(sock, data, 10, 0, (struct sockaddr *) destAddr, destLen) < 0)
			fatalError("Couldn't send packet");

		FD_ZERO(&readfds);
		FD_SET(sock, &readfds);

		tv.tv_sec = TIMEOUT_LENGTH_SEC;
		tv.tv_usec = TIMEOUT_LENGTH_USEC;

		// Block until an event happens (Timeout or data on socket)
		rv = select(sock + 1, &readfds, NULL, NULL, &tv);

		// Did we get an ACK?
		sentAck = getAckNum(sock, &readfds);
	}
}

/*
 * Returns -1 if a timeout occurred or a corrupt ACK was received.
 * Otherwise returns the sequence number of the received ACK
 */
int getAckNum(int sock, fd_set *readfds) {
	char receiveBuf[BUFFER_SIZE];
	struct sockaddr_in fromAddr;
	socklen_t fromLen = sizeof(fromAddr);

	// Do we have data waiting on our socket?
	if (FD_ISSET(sock, readfds)) {
		// Check seq num
		if (recvfrom(sock, receiveBuf, BUFFER_SIZE, 0, (struct sockaddr *) &fromAddr, &fromLen) < 0)
			fatalError("Error while receiving data");

		// Check if this packet is corrupt and get the sequence number
		int corrupt = (receiveBuf[0] == '1');
		int ackNum = (receiveBuf[1] == '1');

		if (!corrupt) {
			return ackNum;
		}
		else
			return -1;
	}
	else
		return -1;
}
