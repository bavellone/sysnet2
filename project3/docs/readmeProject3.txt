In our project we successfully coded a UDP token ring bulletin board. 

Overview:
A server starts by listening for a number of clients provided by the user to connect. After all the clients 
connect, the server sends back the address of another client creating a ring. 
After the ring is created the server exits. The clients then pick a leader client to start the token, 
after that the clients pass the token around the circle. While the clients are passing 
around the token, the user can then access the message board. The user has the ability to 
list the number of messages, read a certain message, write a message, and exit from the 
message board. When the user exits the message board the other clients will then restructure
the ring with one less client so the ring remains unbroken. 

Compile:
To run the server code you must run the command "make all"
Then ./bbserver "number of clients"

To run the client:
./bbpeer "text file name" "server IP" "server Port"