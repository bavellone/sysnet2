#include "common.h"

int handleAnnounce(Comm_Packet);
void sendWaitingPeers();
void sendPeerNeighbors();
Peer getNeighbor(int);
void serializeNeighbor(char *, Peer);