#include "bbPeer.h"

Peer targetPeer; // Keep track of our neighbor
int leaderVote; // Keep track of our vote

int userAction = USER_ACTION_NONE; // Keeps track of what action the user wants to perform
int leaveNetwork = 0; // Indicates whether we should keep listening for packets
char *filename; // The BBfile to read from / write to
int sock; // Socket FD
int DEBUG = 0;
sem_t Token; // Synchronizes the Network and UI thread


int main(int argc, char *argv[]) {
	if (argc < 4) {
		fprintf(stderr, "usage: bbPeer <bbFilename> <serverIP> <serverPort>\n");
		exit(1);
	}

	// Open a new socket on our machine
	sock = openSocket();
	
	// Initialize some things
	filename = argv[1];
	srand(time(NULL));
	sem_init(&Token, 0, 0);
	
	if (argc == 5)
		DEBUG = atoi(argv[4]);
	// Test for collisions
	if (DEBUG) {
		// Run some tests to ensure the randomness on our system is suitable
		runTests();
		return 0;
	}

	// Connect to the server
	struct sockaddr_in *serverAddr = connectToAddr(argv[2], atoi(argv[3]));
	printf("Connection to server established\n");
	
	// Create address to listen on
	struct sockaddr_in *addr = listenAddr();

	// Bind our socket to the address we are listening on
	if (bind(sock, (const struct sockaddr*) addr, sizeof(*addr)) < 0)
		fatalError("Error binding to address");

	// Start the bootstrapping phase
	announce(serverAddr);
	
	// Start the UI thread and start listening for incoming packets
	createThread(startCLI, NULL);
	listenForPackets();
	
	// Gracefully exit
	printf("Goodbye!\n");
	return 0;
}

void runTests() {
	fprintf(stderr, "Running collision tests\n");
	
	int x, y, range = 100000, collisions = 0;
	int test[range];
	
	for (x = 0; x < range; x++) {
		test[x] = genleaderVote();
	}

	fprintf(stderr, "Checking for collisions...\n");
	for (x = 0; x < range; x++) {
		if (x > 0 && x % 10000 == 0) 
			fprintf(stderr, "%i%%...", (int)(x / 1000));
		
		for (y = x; y < range; y++)
			if (x != y && test[x] == test[y]) {
				fprintf(stderr, "\ncollision at [%i] [%i]: %i\n", x, y, test[x]);
				collisions++;
			}
	}
	
	fprintf(stderr, "\nFound %i collisions\n", collisions);
}

/**
* Opens a UDP socket
*/
int openSocket() {
	int sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0)
		fatalError("Error opening socket");
	return sock;
}

/**
* Announce desire to join BB network
*/
void announce(struct sockaddr_in *serverAddr) {
	if (sendCommPacket(PACKET_TYPE_ANNOUNCE, sock, *serverAddr, "Hello World!") < 0)
		fatalError("Could not send Announce Packet!");
}

/**
* Listen for incoming packets
*/
void listenForPackets() {
	char buf[BUFFER_SIZE];
	struct sockaddr_in peerAddr;
	socklen_t size = sizeof(peerAddr);
	
	// Keep listening until we have left the BB Network
	while (!leaveNetwork) {
		memset(buf, 0, BUFFER_SIZE);
		if (recvfrom(sock, buf, BUFFER_SIZE, 0, (struct sockaddr *) &peerAddr, &size) < 0)
			fatalError("Error while receiving data");

		// Got a UDP packet
		handleRequest(newRequest((char *)buf, peerAddr));
	}
}

/**
* Handles incoming UDP data
*/
void handleRequest(Request req) {
	struct comm_packet packet;
	
	parseCommPacket(&packet, req->rawData, *(req->origin));
	processPacket(&packet);

	freeRequest(req);
}

/**
* Creates a Request struct to make it easy to work with incoming UDP data
*/
Request newRequest(char *rawData, struct sockaddr_in origin) {
	int dataLen = strlen(rawData) + 1;
	Request req = malloc(sizeof(struct request));
	req->rawData = malloc(dataLen);
	req->origin = malloc(sizeof(struct sockaddr_in));

	// Copy request data
	strncpy(req->rawData, rawData, dataLen);
	// Copy request origin address
	*req->origin = origin;

	return req; 
}

/**
* Frees the request struct memory
*/
void freeRequest(Request req) {
	free(req->rawData);
	free(req->origin);
	free(req);
}

/**
* We have received a packet and need to perform an action
*/
void processPacket(Comm_Packet packet) {
	if (packet->type == PACKET_TYPE_WAITING_PEERS)
		handleWaitingPeers(packet);
	
	if (packet->type == PACKET_TYPE_NEIGHBOR_ADDR) 
		initiateLeaderElection(packet);
	
	if (packet->type == PACKET_TYPE_LEADER_ELECTION) 
		handleLeader(packet);
	
	if (packet->type == PACKET_TYPE_TOKEN) 
		receivedToken(packet);
}

/**
* We are waiting for more peers to connect
*/
void handleWaitingPeers(Comm_Packet packet) {
	int remainingPeers = atoi(packet->data);
	printf("Waiting for %i more peers to join BB Network...\n", remainingPeers);
}

/**
* Connect to the given IP address and port, then save the connection as our neighbor
*/
void connectTargetPeer(char *IPaddr, int port) {
	// Create address struct
	struct sockaddr_in *addr = connectToAddr(IPaddr, port);

	// Connect to our neighbor
	targetPeer = newPeer(IPaddr, port, *addr);
}

/**
* Start the Leader Election phase
*/
void initiateLeaderElection(Comm_Packet packet) {
	char buf[BUFFER_SIZE];

	// Generate random vote
	leaderVote = genleaderVote();
	snprintf(buf, BUFFER_SIZE, "%i", leaderVote);

	// Extract neighbor address info from packet
	char *IPaddr = strtok(packet->data, ",");
	int port = atoi(strtok(NULL, ","));

	// Connect to our neighbor
	connectTargetPeer(IPaddr, port);
	
	// Send our vote to our neighbor
	if (sendToPeer(targetPeer, sock, PACKET_TYPE_LEADER_ELECTION, (char *)&buf) < 0)
		fatalError("Could not send Leader Token!");
}

/**
* Got a Leader Election packet
*/
void handleLeader(Comm_Packet packet) {
	// Get vote from the packet we received
	int peerVote = atoi(packet->data);

	// We need to throttle here to prevent network saturation and overflows
	throttle();
	
	// Are we the leader
	if (peerVote > leaderVote) {
		// We are not the leader
		if (sendToPeer(targetPeer, sock, PACKET_TYPE_LEADER_ELECTION, packet->data) < 0)
			fatalError("Could not forward Leader vote!");
	}
	if (peerVote < leaderVote) {
		// We are still a candidate
	}
	if (peerVote == leaderVote) {
		// We are the leader!
		// Send the token
		sendToken(packet);
	}
}

/**
* Pulls entropy from /dev/urandom
* Generates a 32-bit random int
* Comparable to the IPv4 address space
*/
int genleaderVote() {
	FILE *prng;
	int vote;
	
	if ((prng = fopen("/dev/urandom", "r")) == NULL)
		fatalError("Error accessing PRNG");

	fread(&vote, 1, sizeof(int), prng); // Grab some random data
	fclose(prng);
	
	return vote;
}

/**
* Got the token packet
*/
void receivedToken(Comm_Packet packet) {
	// We have received the token, allow UI thread to perform action if necessary
	releaseToken();
	// Prevent network saturation and allow UI thread to grab token
	throttle();
	// Block if an action is being performed by the UI thread
	waitForToken();

	// Send the token when pending action is complete
	sendToken(packet);  
}

/**
* Check if we need to perform any special actions or just forward the token
*/
void sendToken(Comm_Packet packet) {
	char buf[BUFFER_SIZE] = { 0 };
	// Check if a peer wants to exit the network
	int exitStatus = checkExit(packet);
	
	// Does user want to exit, and are we the only peer who wants to exit?
	if (userAction == USER_ACTION_EXIT && exitStatus == TOKEN_NO_DATA) {
		// Send data along with the token to tell other peers we are leaving BB network
		snprintf(buf, BUFFER_SIZE, "%i,%s,%i", getCommPort(), targetPeer->IPaddr, targetPeer->port);
		userAction = USER_ACTION_NONE;
	}
	// Do we need to forward the token data?
	else if (exitStatus == TOKEN_FORWARD_DATA || exitStatus == TOKEN_TARGET_PEER_EXITING) {
		// Forward the token along with the original data
		snprintf(buf, BUFFER_SIZE, "%s", packet->data);
	}
	
	// Send the token packet
	if (sendToPeer(targetPeer, sock, PACKET_TYPE_TOKEN, buf) < 0)
		fatalError("Could not send token!");

	// Is our neighbor leaving?
	if (exitStatus == TOKEN_TARGET_PEER_EXITING) {
		// We need to send to another peer to maintain the ring after our target peer leaves
		retargetPeer(packet);
	}
	// Are we allowed to leave the network?
	else if (exitStatus == TOKEN_EXIT) {
		// We can now safely leave the BB network
		leaveNetwork = 1;
	}
}

/**
* Check the token packet to see if we need to take any special actions
*/
int checkExit(Comm_Packet packet) {
	// Is this a token packet and does it have any associated data?
	if (packet->type == PACKET_TYPE_TOKEN && strlen(packet->data) > 0) {
		// Get the port of the peer who wants to leave
		int leavingPeerPort = atoi(strtok(packet->data, ","));
		
		// Did we send the token?
		if (leavingPeerPort == getCommPort()) {
			return TOKEN_EXIT;
		}
		
		// Is our targetPeer leaving the ring?
		else if (leavingPeerPort == targetPeer->port) {
			return TOKEN_TARGET_PEER_EXITING;
		}
		
		// Forward the packet
		else {
			return TOKEN_FORWARD_DATA;
		}
	}
	
	return TOKEN_NO_DATA;
}

/**
* Updates who we send our packets to
*/
void retargetPeer(Comm_Packet packet) {
	freePeer(targetPeer);
	char *IPaddr = strtok(NULL, ",");
	int port = atoi(strtok(NULL, ","));
	connectTargetPeer(IPaddr, port);
}

/**
* Returns the port we are listening on
*/
int getCommPort() {
	struct sockaddr_in addr;
	socklen_t size = sizeof(struct sockaddr);
	getsockname(sock, (struct sockaddr *) &addr, &size);
	
	return ntohs(addr.sin_port);
}

/**
* Runs as the UI thread. Waits for user input and synchronizes with Network thread
*/
void startCLI() {
	int choice;

	// Keep looping until user wants to exit
	while (userAction != USER_ACTION_EXIT) {
    // Print list of available user actions
		printf("Select an action:\n");
		printf("1 - List\n");
		printf("2 - Read\n");
		printf("3 - Write\n");
		printf("4 - Exit\n");
		
		// Gets the action the user wants
		scanf("%i", &choice);

		// Performs the requested action
		performAction(choice);
	}
	
	puts("Leaving BB network...");
	pthread_exit(0);
}

/**
* Will block until token becomes available
*/
void waitForToken() {
	// Block until we have the token
	sem_wait(&Token);
}

/**
* Will release the token for another thread to claim
*/
void releaseToken() {
	// Release token to next thread that needs it
	sem_post(&Token);
}

/**
* Performs the desired user action, or prints an error message if an invalid action is chosen
*/
void performAction(int action) {
	switch (action) {
		case USER_ACTION_LIST:
			// Perform list action
			executeList();
	    resetUserAction();
			break;
		case USER_ACTION_READ:
			// Perform read action
			executeRead();
	    resetUserAction();
			break;
		case USER_ACTION_WRITE:
			// Perform write action
			executeWrite();
	    resetUserAction();
			break;
		case USER_ACTION_EXIT:
			// User wants to exit
			userAction = USER_ACTION_EXIT;
			break;
		default:
	    printf("Please choose a valid action\n");
	}
}

/**
* Reads the shared file and parses it into an array of Message structs for easy use by user actions functions. Will block until token is received.
*/
Message *getMesssages() {
	long fileLen;
	char *fileStr;
	FILE *fileP;

	// We cannot read the file until we have the token
	waitForToken();

	// Open the file in read+write mode, and create the file if it does not exist
	if ((fileP = fopen(filename, "a+")) == NULL)
		fatalError("Error reading shared file");

	fseek(fileP, 0, SEEK_END); // Move pointer to end of file
	fileLen = ftell(fileP); // How many long is the file?
	fseek(fileP, 0, SEEK_SET); // Move pointer to beginning of file

	// Allocate enough memory for the file contents
	fileStr = malloc(sizeof(char) * (fileLen + 1));
	// Clear the memory
	memset(fileStr, 0, fileLen + 1);
	
	// Read file and close it
	fread(fileStr, 1, fileLen, fileP);
	fclose(fileP);

	// Parse string into messages
	Message *messages = parseFileStr(fileStr);
	free(fileStr);

	// We have read the file and no longer need the token
	releaseToken();
	return messages;
}

/**
* Accepts a string and parses it into an array of Messages
*/
Message *parseFileStr(char *fileStr) {
	char *buf, *msgNum, *msgStr;
	int allocated = 0, memLen = (sizeof(struct message) * MAX_MESSAGE_NUM);
	
	// Allocate memory for message array
	Message *messages = malloc(memLen);
	memset(messages, 0, memLen);

	// Tokenize string
	buf = strtok(fileStr, "<>=\n");
	while (buf != NULL) {
		// Grab the tokens we are interested in
		msgNum = strtok(NULL, "<>=\n");
		msgStr = strtok(NULL, "<>=\n");
		
		// Make sure we are within our memory bounds
		if (allocated < MAX_MESSAGE_NUM) 
			messages[allocated++] = newMessage(atoi(msgNum), msgStr);
		else
			fatalError("Too many messages!");

		// Discard extra tokens
		strtok(NULL, "<>=\n");
		buf = strtok(NULL, "<>=\n");
	}

	return messages;
}

/**
* Creates a new Message from the given sequence number and message body
*/
Message newMessage(int msgNum, char *msgStr) {
	Message msg = malloc(sizeof(struct message));
	int len = strlen(msgStr) + 1;
	
	// Copy data to message
	msg->msgNum = msgNum;
	msg->msgStr = malloc(sizeof(char) * len);
	strncpy(msg->msgStr, msgStr, len);

	return msg;
}

/**
* Frees memory associated with Message
*/
void freeMessage(Message msg) {
	free(msg->msgStr);
	free(msg);
}

/**
* Frees an array of messages
*/
void freeMessages(Message *messages) {
	Message msg;
	int x = 0;

	while ((msg = messages[x++]) != NULL) {
		freeMessage(msg);
	}
	free(messages);
}

// Prints all Message sequence numbers
void executeList() {
	Message *messages = getMesssages();
	
	Message msg;
	int x = 0;
	printf("Available messages:\n");
	while ((msg = messages[x++]) != NULL) {
		printf("%i ", msg->msgNum);
		// Put them into an easy-to-read format
		if (x % 10 == 0) {
			printf("\n");
		}
	}
	printf("\n");
	
	freeMessages(messages);
}

/**
* Prints the contents of the selected message
*/
void executeRead() {
	int readMsg, x = 0, found = 0;
	printf("Enter the message number you would like to read: ");
	scanf("%i", &readMsg);

	Message *messages = getMesssages();

	Message msg;
	printf("\n");
	while ((msg = messages[x++]) != NULL) {
		// Is this the message the user wants to read
		if (msg->msgNum == readMsg) {
			printf("%s\n", msg->msgStr);
			found = 1;
		}
	}
	if (!found) {
		printf("Message %i not found!\n", readMsg);
	}
	
	freeMessages(messages);
}

/**
* Accepts user input and writes it to the shared file
*/
void executeWrite() {
	char msgStr[BUFFER_SIZE];
	
	printf("Enter your message (character limit: %i):\n", BUFFER_SIZE);
	// Clear the buffer before getting user message
	clearBuffer();
	fgets(msgStr, BUFFER_SIZE, stdin);
	
	int x = 0;
	Message *messages = getMesssages();
	Message msg;
	while ((msg = messages[x++]) != NULL) {
		continue; // x is keeping track of highest msgNum
	}
	freeMessages(messages);

	// Write the message
	writeBBMessage(x, (char *)&msgStr);
}

/**
* Performs the File IO to write the user message to the shared file
*/
void writeBBMessage(int msgNum, char *msgStr) {
	char buf[BUFFER_SIZE + 26]; // Account for message format characters
	snprintf(buf, BUFFER_SIZE, "<message n=%i>\n%s</message>\n", msgNum, msgStr);
	
	// We need the token before we can write
	waitForToken();
	
	FILE *fileP = fopen(filename, "a+");
	fprintf(fileP, "%s", buf);
	fclose(fileP);

	// Done writing new message, release token
	releaseToken();
}

// Clears the buffer of unwanted newline chars
void clearBuffer() {
	while (getchar() != '\n');
}

/**
* Sets the user action back to NONE
*/
void resetUserAction(){
	userAction = USER_ACTION_NONE;
}

// Sleep for 250ms to throttle network usage to a reasonable speed
// and prevent kernel network buffer overflows 
void throttle() {
	usleep(250000); 
}