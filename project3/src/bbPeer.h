#include "common.h"
#include "thread.h"
#include <semaphore.h>
#include <time.h>
#include <math.h>

// Used in the UI thread
#define USER_ACTION_NONE 0
#define USER_ACTION_LIST 1
#define USER_ACTION_READ 2
#define USER_ACTION_WRITE 3
#define USER_ACTION_EXIT 4

// Set a limit on how many messages are allowed
#define MAX_MESSAGE_NUM 100

// Used in the Network thread
#define TOKEN_NO_DATA 0
#define TOKEN_FORWARD_DATA 1
#define TOKEN_TARGET_PEER_EXITING 2
#define TOKEN_EXIT 3

typedef struct request {
	int replySock;
	char *rawData;
	struct sockaddr_in *origin;
} *Request;

typedef struct message {
	int msgNum;
	char *msgStr;
} *Message;

int openSocket();
void runTests();
void handleWaitingPeers(Comm_Packet);
void connectTargetPeer(char *, int);
void retargetPeer(Comm_Packet);
void listenForPackets();
void handleRequest(Request);
Request newRequest(char *, struct sockaddr_in);
void freeRequest(Request);
void processPacket(Comm_Packet);
void announce(struct sockaddr_in*);
void initiateLeaderElection(Comm_Packet);
void handleLeader(Comm_Packet);
int genleaderVote();
void receivedToken(Comm_Packet);
void sendToken(Comm_Packet);
void releaseToken();
void waitForToken();
void performAction(int);
void resetUserAction();
void clearBuffer();
void startCLI();
void executeWrite();
void executeRead();
void executeList();
char **tokenizer();
Message *getMesssages();
Message *parseFileStr(char *);
Message newMessage(int , char *);
void freeMessage(Message);
void freeMessages(Message *);
void writeBBMessage(int, char *);
void throttle();
int checkExit(Comm_Packet);
int getCommPort();